/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import fileio from '@ohos.fileio';
import { XZData } from './XZDataTest';
import featureAbility from '@ohos.ability.featureAbility';
import { File, IOUtils, InputStream, XZCompressorInputStream } from '@ohos/commons-compress';
import { describe, beforeAll, beforeEach, afterEach, afterAll, it, expect } from "hypium/index";

export default function xzCompressorInputStreamTest() {

  describe('XZCompressorInputStreamTest', function () {

    it('XZCompressorInputStreamTest', 0, function (done) {
      redundantTestOfAlmostDeprecatedMatchesMethod()
      done()
    })

    it('singleByteReadConsistentlyReturnsMinusOneAtEofNoDecompressConcatenated', 0, function (done) {
      var context = featureAbility.getContext();
      context.getFilesDir()
        .then((data) => {
          generateTextFile(data, '/bla.tar.xz', XZData);
          singleByteReadConsistentlyReturnsMinusOneAtEof(false, data);
          done()
        })
        .catch((error) => {
          console.error('File to obtain the file directory. Cause: ' + error.message);
          done()
        })
    })

    it('singleByteReadConsistentlyReturnsMinusOneAtEofDecompressConcatenated', 0, function (done) {
      var context = featureAbility.getContext();
      context.getFilesDir()
        .then((data) => {
          generateTextFile(data, '/bla.tar.xz', XZData);
          singleByteReadConsistentlyReturnsMinusOneAtEof(true, data)
          done()
        })
        .catch((error) => {
          console.error('File to obtain the file directory. Cause: ' + error.message);
          done()
        })
    })

    it('multiByteReadConsistentlyReturnsMinusOneAtEofNoDecompressConcatenated', 0, function (done) {
      var context = featureAbility.getContext();
      context.getFilesDir()
        .then((data) => {
          generateTextFile(data, '/bla.tar.xz', XZData);
          multiByteReadConsistentlyReturnsMinusOneAtEof(false, data);
          done()
        })
        .catch((error) => {
          console.error('File to obtain the file directory. Cause: ' + error.message);
          done()
        })
    })

    it('XZCompressorInputStreamTest4', 0, function (done) {
      var context = featureAbility.getContext();
      context.getFilesDir()
        .then((data) => {
          generateTextFile(data, '/bla.tar.xz', XZData);
          multiByteReadConsistentlyReturnsMinusOneAtEof(true, data);
          done()
        })
        .catch((error) => {
          console.error('File to obtain the file directory. Cause: ' + error.message);
          done()
        })
    })
  })
}

function redundantTestOfAlmostDeprecatedMatchesMethod() {
  let data: Int8Array = new Int8Array([-3, 55, 122, 88, 90, 0]);

  expect(false).assertFalse(XZCompressorInputStream.matches(data, 5));
  expect(true).assertTrue(XZCompressorInputStream.matches(data, 6));
  expect(true).assertTrue(XZCompressorInputStream.matches(data, 7));
  data[5] = 48;
  expect(false).assertFalse(XZCompressorInputStream.matches(data, 6));
}


function singleByteReadConsistentlyReturnsMinusOneAtEof(decompressConcatenated: boolean, data: string) {

  let input2 = new File(data + "/xz", "bla.tar.xz");
  let input3: InputStream = new InputStream();
  input3.setFilePath(input2.getPath());
  let xzCompressorInputStream: XZCompressorInputStream = new XZCompressorInputStream(input3, decompressConcatenated, -1);
  IOUtils.toByteArray(xzCompressorInputStream);
  expect(xzCompressorInputStream.read() == -1).assertTrue();
  expect(xzCompressorInputStream.read() == -1).assertTrue();
  xzCompressorInputStream.close();
  input3.close()

}

function multiByteReadConsistentlyReturnsMinusOneAtEof(decompressConcatenated: boolean, data: string) {

  let input2 = new File(data + "/xz", "bla.tar.xz");
  let input3: InputStream = new InputStream();
  input3.setFilePath(input2.getPath());
  let buf: Int8Array = new Int8Array(2);
  let xzCompressorInputStream: XZCompressorInputStream = new XZCompressorInputStream(input3, decompressConcatenated, -1);
  IOUtils.toByteArray(xzCompressorInputStream);
  expect(-1 == xzCompressorInputStream.readBytes(buf)).assertTrue();
  expect(-1 == xzCompressorInputStream.readBytes(buf)).assertTrue();
  xzCompressorInputStream.close();
  input3.close()

}

function generateTextFile(data: string, fileName: string, arr: Int8Array | Int32Array): void {
  let srcPath = data + '/xz';
  try {
    fileio.mkdirSync(srcPath);
  } catch (err) {
  }
  const writer = fileio.openSync(srcPath + fileName, 0o102, 0o666);
  fileio.writeSync(writer, arr.buffer);
  fileio.closeSync(writer);
}
