/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Long, XxHash64 } from '@ohos/commons-compress';
import { describe, beforeAll, beforeEach, afterEach, afterAll, it, expect } from "hypium/index";

export default function ZSTDXxHash64Test() {
  let PRIME = Long.fromNumber(2654435761);
  let ARRAY_BYTE_BASE_OFFSET = Long.fromNumber(16)
  let buffer: Int8Array = new Int8Array(101);

  function TestXxHash64() {
    let value: Long = PRIME;
    for (let i = 0; i < buffer.length; i++) {
      buffer[i] = value.shiftRight(24).toInt();
      value = value.multiply(value);
    }
  }

  describe('ZSTDXxHash64Test', function () {

    it('ZSTDTestSanity', 0, function (done) {
      TestXxHash64()
      testSanity()
      done()
    })
  })

  function testSanity() {
    assertHash(Long.fromNumber(0), buffer, 0, Long.fromString('-1205034819632174695'));

    assertHash(Long.fromNumber(0), buffer, 1, Long.fromString('5750596776143442648'));
    assertHash(PRIME, buffer, 1, Long.fromString('8329478753618994979'));

    assertHash(Long.fromNumber(0), buffer, 4, Long.fromString('-7901876112562082063'));
    assertHash(PRIME, buffer, 4, Long.fromString('708753852729961291'));

    assertHash(Long.fromNumber(0), buffer, 8, Long.fromString('-626931337744172849'));
    assertHash(PRIME, buffer, 8, Long.fromString('-7186417346120842555'));

    assertHash(Long.fromNumber(0), buffer, 14, Long.fromString('-3460297540090709443'));
    assertHash(PRIME, buffer, 14, Long.fromString('6599481375206459851'));

    assertHash(Long.fromNumber(0), buffer, 32, Long.fromString('-5812084625956540946'));
    assertHash(PRIME, buffer, 32, Long.fromString('-2545780413506618609'));

    assertHash(Long.fromNumber(0), buffer, buffer.length, Long.fromString('1057031117799454893'));
    assertHash(PRIME, buffer, buffer.length, Long.fromString('-3844287129753543135'));
  }

  function assertHash(seed: Long, data: Int8Array, length: number, expected: Long) {
    expect(hash(seed, data, length).eq(expected)).assertTrue();
  }

  function hash(seed: Long, data: Int8Array, length: number): Long {
    return XxHash64.hash(seed, data, ARRAY_BYTE_BASE_OFFSET, length);
  }
}
